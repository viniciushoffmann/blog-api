class CreatePostsTagsJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_join_table :posts, :tags do |t|
      t.references :post
      t.references :tag
    end
  end
end
