class AddCommentToComment < ActiveRecord::Migration[5.1]
  def change
    add_reference :comments, :comment
  end
end
