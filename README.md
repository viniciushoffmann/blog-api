# Blog API

Exemplo de API para Blog desenvolvida em Ruby on Rails (5.1.7) específica para o treinamento de novos desenvolvedores do Scopi. Aplicação gerada como uma Rails API, não possuindo view templates e apenas renderizando json.

## Configuração do ambiente de desenvolvimento:
```
$ bundle install
$ rake db:create
$ rake db:migrate
$ rake db:seed
$ rails server
```

## Rotas disponíveis
```
$ rake routes
```

Futuras implementações serão sugeridas e devem ser submetidas através de pull request para uma branch específica do desenvolvedor.
```
$ git checkout -b <branch_name>
```

Não será permitida a sincronização da branch do desenvolvedor para a branch master, sem aprovação através de pull request.
```
$ git add .
$ git commit -m "Texto explicativo do commit"
$ git push origin <branch_name>
```
