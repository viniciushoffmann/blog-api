class TagsController < ApplicationController
  before_action :set_tag, only: [ :show, :update, :destroy ]

  # GET /tags
  def index
    @tags = Tag.all
    render json: @tags
  end


  # POST /tags
  def create
    @tag = Tag.new(tag_params)
    if @tag.save
      render json: @tag, status: :created
    else
      render json: @tag.errors, status: :unprocessable_entity
    end
  end

  # GET /tags/:id
  def show
    render json: @tag.index_info_tag
  end

  # PATCH/PUT /tags/:id
  def update
    if @tag.update(tag_params)
      render json: @tag, status: :ok
    else
      render json: @tag.errors, status: :unprocessable_entity
    end
  end

  # delete /tags/:id
  def destroy
    @tag.destroy
    head :no_content
  end
  
  
  private
    def set_tag
      @tag = Tag.find(params[:id])
    end

    def tag_params
      params.require(:tag).permit(:name)
    end
end
