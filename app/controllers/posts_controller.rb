class PostsController < ApplicationController
  before_action :authorize_request, except: :create
  before_action :set_post, only: [ :show, :update, :destroy, :link_tag, :unlink_tag, :replace_tag]
  before_action :set_tag, only: [ :create, :link_tag, :unlink_tag, :update]


  # GET /posts    aqui add os filtros
  def index
    @posts = Post.joins(:tags).distinct #pega todos os posts q se tem 

    if params[:titulo].present? # o present com o ? e pra ver se foi informado algum valor
      @posts = @posts.where(title: params[:titulo]) #filtra por titulo  
    end
    
    if params[:descricao].present? # o present com o ? e pra ver se foi informado algum valor
      @posts = @posts.where(description: params[:descricao]) #filtra por descricao
    end

    if params[:tags].present? # o present com o ? e pra ver se foi informado algum valor
      @posts =  @posts.where(:tags => {name: params[:tags]}) #pega todos os posts pela tag associada
    end
    
    if params[:comments].present? # o present com o ? e pra ver se foi informado algum valor
      @posts =  @posts.joins(:comments).where(:comments => {text: params[:comments]}) #pega todos os posts com esse comentario
    end
    
    if params[:page].present?
      @posts = @posts.page(params[:page]).per_page(3)#:page => params[:page]
    end

    posts =[]
    @posts.each do |t|
      posts << t.index_info
    end
        
    render json: posts
    
    #render json: Post.list
    
  end

  # POST /posts
  def create
    @post = Post.new(post_params)
    if @post.save
      @post.tags.push(@tag)
      render json: @post.index_info, status: :created
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  # GET /posts/:id
  def show
    # render json: @pos
    render json: @post.index_info # aqui manda imprimir os atributos q estao declarados no post.rb/index_info
  end

  # PATCH/PUT /posts/:id
  def update
    if @post.update(post_params)
      @post.tags.push(@tag)
      render json: @post, status: :ok
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  # DELETE /posts/:id
  def destroy
    @post.destroy
    head :no_content
  end

  # POST /posts/:id/tag
  def link_tag
    @post.tags.push(@tag)
    render json: @post.tags, status: :ok
  end
  
  # DELETE /posts/:id/tag
  def unlink_tag
    @post.tags.delete(@tag)
    head :no_content
  end

  def replace_tag
    @post.tags.delete_all
    params[:tag_ids].each do |t|
    @tag = Tag.find(t) 
      if @tag
        @post.tags.push(@tag)
      end
    end  
    render json: @post.tags, status: :ok
  end


  private

    def post_params
      params.require(:post).permit(:title, :description, :user_id)# para adicionar um user_id ao post
    end

    def tag_params
      params.require(:post).permit(:tag_id, :tag_ids) #aqui ele permite q seja inseridos esses valores ao post "tem q ter um desses"
    end

    def set_post
      @post = Post.find(params[:id])
    end

    def set_tag
      @tag = Tag.find(tag_params[:tag_id])
    end

end
