class ApplicationController < ActionController::API
  # protect_from_forgery with: :exception
  # protect_from_forgery with: :null_session
  # rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
  # rescue_from ActiveRecord::RecordNotUnique, with: :render_not_unique

  def not_found
    render json: { error: 'not_found' }
  end

  def authorize_request
    header = request.headers['Authorization']
    header = header.split(' ').last if header
    begin
      @decoded = JsonWebToken.decode(header)
      @current_user = User.find(@decoded[:user_id])
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: e.message }, status: :unauthorized
    rescue JWT::DecodeError => e
      render json: { errors: e.message }, status: :unauthorized
    end
  end

  private

  def render_not_found(exception)
    render json: { error: exception.message }, status: :not_found
  end
  
  def render_not_unique(exception)
    render json: { error: "Already been taken" }, status: :unprocessable_entity
  end

end
 