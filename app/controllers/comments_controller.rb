class CommentsController < ApplicationController  
  before_action :set_post, except:[ :comment, :update, :destroy] #aqui ele passa pelos metodos q nao precisa
  before_action :set_comment, only:[ :show, :update, :destroy, :comment ]

  # GET /posts/:post_id/comments
  def index

    @comments = @post.comments

    comments=[]
    @comments.each do |t|
      comments << t.index_info_comment
    end

    render json: comments

  end
  

  # POST /posts/:post_id/comments
  def create
    @comment = @post.comments.new(comment_params) 
    if @comment.save #aqui testa pra ver se ja tem um com esse nome
      render json: @comment.index_info_comment, status: :created # aqui cria o arquivo
    else
      render json: @comment.errors, status: :unprocessable_entity
    end    
  end

  def show
    render json: @comment.index_info_comment
  end

  def update
    if @comment.update(comment_params)
      render json: @comment, status: :ok
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  def destroy
    
    @comment.destroy
    head :no_content
  end

  def comment
    @new_comment = @comment.comments.new(comment_params)
    if @new_comment.save
      render json: @new_comment, status: :created
    else
      render json: @new_comment.errors, status: :unprocessable_entity
    end 
  end

  private
    def set_post
      @post = Post.find(params[:post_id])
    end

    def comment_params
      params.require(:comment).permit(:text, :user_id)  #aqui ele permite q seja inseridos esses valores ao comment "tem q ter um desses", e insere um usuario ao comment q deseja
      
    end

    def set_comment
      @comment = Comment.find(params[:id])
    end

end
