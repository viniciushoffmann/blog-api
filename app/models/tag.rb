class Tag < ApplicationRecord
  has_and_belongs_to_many :posts, dependent: :destroy
  validates :name, presence: true, uniqueness: true, length: { maximum: 20 }

  def index_info_tag
    return {
      id: self.id,
      name: self.name
    }
  end
end
