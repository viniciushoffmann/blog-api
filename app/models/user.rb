class User < ApplicationRecord
  has_many :comment, dependent: :nullify
  has_many :post, dependent: :nullify
  has_secure_password
  
  
  validates :name, presence: true, uniqueness: true
  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :username, presence: true, uniqueness: true
  validates :password, length: { minimum: 6 }, if: -> { new_record? || !password.nil? }
 
end