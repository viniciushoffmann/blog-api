class Comment < ApplicationRecord
  
  belongs_to :post ,optional: true
  belongs_to :user ,optional: true
  belongs_to :comment ,optional: true
  has_many :comments ,foreign_key: :comment_id, dependent: :destroy
  validates :text, presence: true

  #default_scope -> { all.order('updated_at desc') }

  
  def index_info_comment
    return {
      id: self.id,
      text: self.text,
      created_at: self.created_at,
      comments: get_comments(self.comments), 
      user: self.user     
    }
  end

  def get_comments comments
    response = []
    comments.each do |c|
      response << c.index_info_comment
    end
    return response
  end
end
  