class Post < ApplicationRecord
  has_many :comments, dependent: :destroy
  belongs_to :user, optional: true
  has_and_belongs_to_many :tags, before_add: :check_tag

  validates :title, length: { within: 1..255 }, uniqueness: true # o titulo tem q ter o tamanho entre 1 e 255, e tem q existir
  validates :description, length: { minimum: 10 }
  
  def index_info
    return {
      id: self.id,
      title: self.title,
      description: self.description,
      created_at: self.created_at,
      updated_at: self.updated_at,
      comments: self.pega_comment, #aqui esta chamando os comentario e os comentarios dos comentarios
      tags: self.tags,
      user: self.user
    }
  end

  def pega_comment
    @count = []
    self.comments.each do |t|        #percore todos os comments 
      @count << t.index_info_comment #esta decementando o count q recebeu o bloco e passnado o index info para a variavel cont
      #pega_comment_filhos(t)         #chama a funçao para pegar o filho do comentario
    end
    return @count                    #aqui ele retorna todos o bloco com todos os comentairos tanto filhos como os comentarios pais
  end

  def pega_comment_filhos(comment)   #passa o parametro q comment q ele recebeu no metodo pega_comment
    comment.comments.each do |t|     #percore todo o bloco do comentario dos comentarios
      @count << t.index_info_comment                    #decrementa o bloco recebido
      pega_comment_filhos(t)         #chama de novo a funçao para ver se tem mais comentarios filhos
    end
  end
    

  # verifica se a tag já está vinculada ao post
  def check_tag(tag)
    ## desta forma apenas executa um rollback, não causa exceção
    # raise ActiveRecord::Rollback if self.tags.include?(tag)

    ## desta forma causa uma exceção, tratada em ApplicationController
    raise ActiveRecord::RecordNotUnique if self.tags.include?(tag)
  end

  def self.list
    return Post.includes(:comments).all().map do |post|
      post.index_info
    end
  end
end
