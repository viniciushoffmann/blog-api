Rails.application.routes.draw do
  

  resources :users, param: :_username
  post '/auth/login', to: 'authentication#login'
  #get '/*a', to: 'application#not_found'
   
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :posts, except: [ :new, :edit ] do
    resources :comments, except: [ :new, :edit]
  end
  
  delete "/users/:id", controller: :users, action: :destroy
  put "/users/:id", controller: :users, action: :update

  delete "/posts/:id", controller: :posts, action: :destroy

  post "/posts/:id/tag", controller: :posts, action: :link_tag
  delete "/posts/:id/tag", controller: :posts, action: :unlink_tag

  post "/posts/:id/replace_tag", controller: :posts, action: :replace_tag
  
  # criando essa rota para poder acessar e criar os comentarios de comentarios
  post "/comments/:id/comment", controller: :comments, action: :comment 
  delete "/comments/:id", controller: :comments, action: :destroy 

  resources :tags, except: [ :new, :edit ]

  # rota para a validação dos usuarios


end
